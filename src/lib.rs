#[macro_use] extern crate lazy_static;
extern crate regex;

use std::sync::mpsc::{Sender, Receiver, channel, RecvTimeoutError};
use std::thread;
use std::thread::{JoinHandle};
use std::cmp;
use rusqlite::{Connection, NO_PARAMS};
use rusqlite::types::ValueRef;
use chrono::prelude::*;
use regex::{Regex, RegexBuilder};


#[derive(Clone)]
pub struct Reminder {
    id: Option<i32>,
    message: String,
    time: DateTime<Local>,
}


impl Reminder {
    pub fn trigger_time(&self) -> DateTime<Local> { self.time }
}


fn wait_for_reminders(sqlite: Connection, new_reminders: Receiver<Reminder>, callback: Sender<String>) {
    let mut add_reminder = sqlite.prepare("INSERT INTO reminders (message, deadline) VALUES (?1, ?2)")
        .expect("Failed to prepare add_reminder statement");

    let mut find_ready_reminders = sqlite.prepare("SELECT id, message FROM reminders WHERE datetime(deadline) <= datetime('now')")
        .expect("Failed to prepare find_ready_reminders statement");

    let mut clear_reminder = sqlite.prepare("DELETE FROM reminders WHERE id = ?1")
        .expect("Failed to prepare clear_reminder statement");
        
    let mut next_ready_time = sqlite.prepare("SELECT MIN(deadline) FROM reminders")
        .expect("Failed to prepare clear_reminder statement");
    
    loop {
        let ready_reminders = find_ready_reminders.query_map(NO_PARAMS, |row| Reminder {
            id: Some(row.get(0)),
            message: row.get(1),
            time: Local::now()
        });

        let ready_reminders = match ready_reminders {
            Ok(r) => r,
            Err(e) => {
                panic!("Couldn't execute query: {:?}", e);
            }
        };

        let mut ids_to_remove = vec![];

        for reminder in ready_reminders {
            match reminder {
                Ok(r)  => {
                    if callback.send(r.message).is_err() {
                        println!("Reminder callback channel closed");
                        panic!();
                    }
                    match r.id {
                        Some(id) => ids_to_remove.push(id),
                        None     => {}
                    }
                }
                Err(e) => println!("Error unwrapping result: {:?}", e)
            }
        }

        for id in ids_to_remove {
            match clear_reminder.execute(&[&id]) {
                Ok(count) => println!("Cleared {}", count),
                Err(e)    => println!("Error deleting reminder: {:?}", e)
            }
        }

        let mut time = next_ready_time.query(NO_PARAMS).expect("Couldn't query for next ready time");
        let time = match time.next() {
            Some(value) => Some(value.expect("Couldn't get next_ready_time result")),
            None        => None
        };

        let time = match time {
            Some(t) => match t.get_raw(0) {
                ValueRef::Text(text) => Some(text),
                _ => None
            }
            None => None
        };

        let reminder = match time {
            Some(t) => match DateTime::parse_from_rfc3339(t) {
                Ok(deadline) => {
                    let time_til_next = deadline.signed_duration_since(Local::now());

                    match time_til_next.to_std() {
                        Ok(timeout) => match new_reminders.recv_timeout(timeout) {
                            Ok(reminder)                        => Some(reminder),
                            Err(RecvTimeoutError::Timeout)      => None,
                            Err(RecvTimeoutError::Disconnected) => panic!("Channel closed! (After waiting with timeout)")
                        },
                        Err(_) => None // this means the deadline is in the past
                    }
                }
                Err(_e) => Some(new_reminders.recv().expect("Channel closed! (After failing to parse datetime)"))
            }
            None => Some(new_reminders.recv().expect("Channel closed! (No pending reminders)"))
        };

        if let Some(reminder) = reminder {
            add_reminder.execute(&[&reminder.message, &reminder.time.to_rfc3339()])
                .expect("Failed to add reminder");
            
            println!("Added reminder <<<{}>>>", reminder.message);
        }
    }
}


fn migrate_db(sqlite: &Connection) -> rusqlite::Result<()> {
    sqlite.execute("CREATE TABLE IF NOT EXISTS reminders (
            id INTEGER PRIMARY KEY, message TEXT NOT NULL, deadline TEXT NOT NULL
        )", NO_PARAMS)?;
    
    Ok(())
}


pub struct Bot {
    _reminder_thread: JoinHandle<()>,
    reminder_sender:  Sender<Reminder>,
}


impl Bot {
    /// Create a new Bot from the given database.
    /// 
    /// `db` is expected to be a sqlite-compatible database string,
    /// so it can be "wahtever.db" or ":memory:".
    pub fn new(db: &str) -> rusqlite::Result<(Bot, Receiver<String>)> {
        let sqlite                     = Connection::open(db)?;
        let (data_sender, data_recver) = channel();
        let (rem_sender, rem_recver)   = channel();

        migrate_db(&sqlite)?;

        Ok((Bot {
            _reminder_thread: thread::spawn(move || wait_for_reminders(sqlite, data_recver, rem_sender)),
            reminder_sender:  data_sender,
        }, rem_recver))
    }


    /// Queues up a reminder to be sent at a given time.
    /// 
    /// You probably want to just send the bot messages rather than use this directly.
    pub fn set_reminder(&mut self, reminder: Reminder) -> Result<(), String> {
        match self.reminder_sender.send(reminder) {
            Ok(()) => Ok(()),
            Err(_) => return Err("Failed to send reminder".to_string())
        }
    }

    /// Queues up a reminder to be sent at a given time based on the given message and time.
    /// 
    /// You probably want to just send the bot messages rather than use this directly.
    pub fn set_reminder_msg_time(&mut self, message: &str, time: &DateTime<Local>) -> Result<(), String> {
        self.set_reminder(Reminder { message: message.to_string(), time: *time, id: None })
    }


    /// Parses a message into a reminder object
    pub fn parse_message(&self, message: &str) -> Option<Reminder> {
        lazy_static! {
            static ref REMIND_ME: Regex = RegexBuilder::new(r"\s*remind\s+me(\s+to|\s+of|\s+about)?\s*")
                .case_insensitive(true)
                .build()
                .expect("Regex REMIND_ME failed to compile");

            static ref IN_N_UNITS: Regex = RegexBuilder::new(r"\s*in\s+(\d+)\s*(minutes?|mins?|seconds?|secs?|hours?|hrs?)\s*")
                .case_insensitive(true)
                .build()
                .expect("Regex IN_N_UNITS failed to compile");

            static ref IN_N_DAYS: Regex = RegexBuilder::new(r"\s*in\s+(\d+)\s*(days?|weeks?|months?)\s*")
                .case_insensitive(true)
                .build()
                .expect("Regex IN_N_DAYS failed to compile");

            static ref ON_WEEKDAY: Regex = RegexBuilder::new(r"\s*on\s+(sunday|monday|tuesday|wednesday|thursday|friday|saturday|sun|mon|tues|wed|thurs|fri|sat)\s*")
                .case_insensitive(true)
                .build()
                .expect("Regex ON_WEEKDAY failed to compile");

            static ref AT_TIME_NO_MINS: Regex = RegexBuilder::new(r"\s*at\s+(\d+)\s*(pm|am)?\s*")
                .case_insensitive(true)
                .build()
                .expect("Regex AT_TIME_NO_MINS failed to compile");

            static ref AT_TIME_MINS: Regex = RegexBuilder::new(r"\s*at\s+(\d+):(\d+)\s*(pm|am)?\s*")
                .case_insensitive(true)
                .build()
                .expect("Regex AT_TIME_MINS failed to compile");

            static ref LEADING_WHITESPACE: Regex = RegexBuilder::new(r"^\s+")
                .build()
                .expect("Regex LEADING_WHITESPACE failed to compile");
            static ref TRAILING_WHITESPACE: Regex = RegexBuilder::new(r"\s+$")
                .build()
                .expect("Regex LEADING_WHITESPACE failed to compile");

            static ref HOURS: Regex = RegexBuilder::new(r"^h")
                .case_insensitive(true).build().unwrap();
            static ref MINUTES: Regex = RegexBuilder::new(r"^min")
                .case_insensitive(true).build().unwrap();
            static ref SECONDS: Regex = RegexBuilder::new(r"^sec")
                .case_insensitive(true).build().unwrap();

            static ref DAYS: Regex = RegexBuilder::new(r"^day")
                .case_insensitive(true).build().unwrap();
            static ref WEEKS: Regex = RegexBuilder::new(r"^week")
                .case_insensitive(true).build().unwrap();
            static ref MONTHS: Regex = RegexBuilder::new(r"^month")
                .case_insensitive(true).build().unwrap();

            static ref MONDAY: Regex = RegexBuilder::new(r"^mon")
                .case_insensitive(true).build().unwrap();
            static ref TUESDAY: Regex = RegexBuilder::new(r"^tue")
                .case_insensitive(true).build().unwrap();
            static ref WEDNESDAY: Regex = RegexBuilder::new(r"^wed")
                .case_insensitive(true).build().unwrap();
            static ref THURSDAY: Regex = RegexBuilder::new(r"^thu")
                .case_insensitive(true).build().unwrap();
            static ref FRIDAY: Regex = RegexBuilder::new(r"^fri")
                .case_insensitive(true).build().unwrap();
            static ref SATURDAY: Regex = RegexBuilder::new(r"^sat")
                .case_insensitive(true).build().unwrap();
            static ref SUNDAY: Regex = RegexBuilder::new(r"^sun")
                .case_insensitive(true).build().unwrap();

            static ref PM: Regex = RegexBuilder::new(r"^pm")
                .case_insensitive(true).build().unwrap();
        }

        let mut msg_start_index = 0;
        let mut msg_end_index = message.len();
        let mut time: Option<DateTime<Local>> = None;
        let mut day_specified = false;
        let now = Local::now();


        // Trim whitespace
        if let Some(caps) = LEADING_WHITESPACE.captures(message) {
            let hit = caps.get(0).unwrap();
            msg_start_index = hit.end();
        }
        if let Some(caps) = TRAILING_WHITESPACE.captures(message) {
            let hit = caps.get(0).unwrap();
            msg_end_index = hit.start()+1;
        }


        // See if "remind me" kicks off the string -- if so, we chop it off
        if let Some(hit) = REMIND_ME.find(message) {
            msg_start_index = hit.end();
        }


        // Helper routine to gradually trim metadata out of the message
        let mut narrow_down_message = |hit: &regex::Match|
            if hit.start() > msg_start_index { msg_end_index = cmp::min(msg_end_index, hit.start()) }
            else { msg_start_index = cmp::max(msg_start_index, hit.end()-1) };


        // Look for "in n minutes"
        if let Some(caps) = IN_N_UNITS.captures(message) {
            let hit = caps.get(0).unwrap();
            narrow_down_message(&hit);

            let amount = caps.get(1).unwrap().as_str().parse::<i64>().unwrap();
            let unit   = caps.get(2).unwrap().as_str();

            if      HOURS.is_match(unit)   { time = Some(now + chrono::Duration::hours(amount)); }
            else if MINUTES.is_match(unit) { time = Some(now + chrono::Duration::minutes(amount)); }
            else if SECONDS.is_match(unit) { time = Some(now + chrono::Duration::seconds(amount)); }

            day_specified = true;
        }


        // Look for "in n days"
        if let Some(caps) = IN_N_DAYS.captures(message) {
            if day_specified { return None }

            let hit = caps.get(0).unwrap();
            narrow_down_message(&hit);

            let amount = caps.get(1).unwrap().as_str().parse::<i64>().unwrap();
            let unit   = caps.get(2).unwrap().as_str();

            if      DAYS.is_match(unit)   { time = Some(now + chrono::Duration::days(amount)); }
            else if WEEKS.is_match(unit)  { time = Some(now + chrono::Duration::weeks(amount)); }
            else if MONTHS.is_match(unit) { time = Some(now + chrono::Duration::days(amount*30)); }

            day_specified = true;
        }


        // Look for "on weekday"
        if let Some(caps) = ON_WEEKDAY.captures(message) {
            if day_specified { return None }

            let hit = caps.get(0).unwrap();
            narrow_down_message(&hit);

            let weekday = caps.get(1).unwrap().as_str();
            let offset_time = |wd| Some(time.unwrap_or(now) + chrono::Duration::days(wd as i64 - now.weekday() as i64));

            if      SUNDAY.is_match(weekday)    { time = offset_time(Weekday::Sun); }
            else if MONDAY.is_match(weekday)    { time = offset_time(Weekday::Mon); }
            else if TUESDAY.is_match(weekday)   { time = offset_time(Weekday::Tue); }
            else if WEDNESDAY.is_match(weekday) { time = offset_time(Weekday::Wed); }
            else if THURSDAY.is_match(weekday)  { time = offset_time(Weekday::Thu); }
            else if FRIDAY.is_match(weekday)    { time = offset_time(Weekday::Fri); }
            else if SATURDAY.is_match(weekday)  { time = offset_time(Weekday::Sat); }

            let deadline = time.unwrap();
            if deadline <= now { time = Some(deadline + chrono::Duration::weeks(1)); }

            day_specified = true;
        }


        // Look for "at Hpm"
        if let Some(caps) = AT_TIME_NO_MINS.captures(message) {
            let hit = caps.get(0).unwrap();
            narrow_down_message(&hit);

            let hour = caps.get(1).unwrap().as_str().parse::<u32>().unwrap();
            let is_pm = match caps.get(2) {
                Some(s) => Some(PM.is_match(s.as_str())),
                None    => None
            };
            let deadline = time.unwrap_or(now);

            time = match is_pm {
                Some(pm) => deadline.with_hour(hour + if pm { 12 } else { 0 }),
                None     => deadline.with_hour(hour)
            };

            // Make sure minutes is 0
            time = match time {
                Some(t) => t.with_minute(0),
                n => n
            };

            // Round up to the nearest time in the future if necessary
            if is_pm.is_none() || !day_specified {
                while time.is_some() && time.unwrap() <= now {
                    time = Some(time.unwrap() + chrono::Duration::hours(if is_pm.is_some() { 24 } else { 12 }));
                }
            }
        }

        // Look for "at H:MMpm"
        if let Some(caps) = AT_TIME_MINS.captures(message) {
            let hit = caps.get(0).unwrap();
            narrow_down_message(&hit);

            let hour   = caps.get(1).unwrap().as_str().parse::<u32>().unwrap();
            let minute = caps.get(2).unwrap().as_str().parse::<u32>().unwrap();
            let is_pm = match caps.get(3) {
                Some(s) => Some(PM.is_match(s.as_str())),
                None    => None
            };
            let deadline = time.unwrap_or(now);

            time = match is_pm {
                Some(pm) => deadline.with_hour(hour + if pm { 12 } else { 0 }),
                None     => deadline.with_hour(hour)
            };

            // Make sure we're including the minute
            time = match time {
                Some(t) => t.with_minute(minute),
                n => n
            };

            // Round up to the nearest time in the future if necessary
            if is_pm.is_none() || !day_specified {
                while time.is_some() && time.unwrap() <= now {
                    time = Some(time.unwrap() + chrono::Duration::hours(if is_pm.is_some() { 24 } else { 12 }));
                }
            }
        }


        if msg_start_index >= msg_end_index {
            return None;
        }

        let message_slice = message[msg_start_index..msg_end_index].to_string();
        match time {
            Some(deadline) => Some(Reminder {
                message: message_slice,
                time:    deadline,
                id:      None
            }),
            None => None
        }
    }


    /// Handles a user message by setting a reminder or possibly
    /// reporting back an error.
    /// The return value is a message that can be sent back to the
    /// user to inform them of whath appened.
    pub fn handle_message(&mut self, message: &str) -> String {
        let reminder = match self.parse_message(message) {
            Some(r) => r,
            None    => {
                return "Didn't quite catch that -- try changing your wording.".to_string();
            }
        };

        match self.set_reminder(reminder.clone()) {
            Ok(())   => format!("OK. Saved for {}", reminder.time.to_rfc2822()),
            Err(msg) => format!("Failed to set reminder. {} I'm probably broken.", msg)
        }
    }
}



//////////////////////////////////////////////////////////
/////////////////////////// Tests ////////////////////////
//////////////////////////////////////////////////////////

#[cfg(test)]
mod test {
    use super::Bot;
    use chrono::prelude::*;
    use std::time;
    use std::ops::Add;

    fn time_approx_eq(t1: DateTime<Local>, t2: DateTime<Local>) -> bool {
        let alpha = chrono::Duration::milliseconds(500);
        (t1 - alpha < t2) && (t1 + alpha > t2)
    }

    #[test]
    fn bot_can_be_instantiated() {
        let bot_result = Bot::new(":memory:");
        assert!(bot_result.is_ok());
    }

    #[test]
    fn bot_can_parse_in_5_minutes() {
        let (bot, _) = Bot::new(":memory:")
            .expect("Couldn't create bot");

        let five_mins = Local::now().add(chrono::Duration::minutes(5));
        
        let result = bot.parse_message("remind me to test the bot in 5 minutes");
        assert!(result.is_some());
        let result = result.unwrap();
        assert_eq!(result.message, "test the bot");
        assert!(time_approx_eq(result.time, five_mins));

        let result = bot.parse_message("remind  me test the bot  again     in  5  Minute");
        assert!(result.is_some());
        let result = result.unwrap();
        assert_eq!(result.message, "test the bot  again");
        assert!(time_approx_eq(result.time, five_mins));

        let result = bot.parse_message("  test the bot again     in 5min");
        assert!(result.is_some());
        let result = result.unwrap();
        assert_eq!(result.message, "test the bot again");
        assert!(time_approx_eq(result.time, five_mins));

        let result = bot.parse_message("in 5 mins remind me to test the bot");
        assert!(result.is_some());
        let result = result.unwrap();
        assert_eq!(result.message, "test the bot");
        assert!(time_approx_eq(result.time, five_mins));
    }

    #[test]
    fn bot_can_parse_on_monday() {
        let (bot, _) = Bot::new(":memory:")
            .expect("Couldn't create bot");

        let result = bot.parse_message("remind me to do a test on Monday");
        assert!(result.is_some());
        let result = result.unwrap();
        assert_eq!(result.message,        "do a test");
        assert_eq!(result.time.weekday(), Weekday::Mon);
        assert!(result.time             > Local::now());

        let result = bot.parse_message("remind me of the test on Tuesday");
        assert!(result.is_some());
        let result = result.unwrap();
        assert_eq!(result.message,        "the test");
        assert_eq!(result.time.weekday(), Weekday::Tue);
        assert!(result.time             > Local::now());

        let result = bot.parse_message("remind me the test ✔✔✔ yes on sat");
        assert!(result.is_some());
        let result = result.unwrap();
        assert_eq!(result.message,        "the test ✔✔✔ yes");
        assert_eq!(result.time.weekday(), Weekday::Sat);
        assert!(result.time             > Local::now());
    }

    #[test]
    fn bot_can_parse_at_5pm() {
        let (bot, _) = Bot::new(":memory:")
            .expect("Couldn't create bot");

        let at_5pm = Local::now().with_hour(12+5).unwrap().with_minute(0).unwrap();
        let at_5_30am = Local::now().with_hour(5).unwrap().with_minute(30).unwrap();

        let result = bot.parse_message("win the game at 5pm");
        assert!(result.is_some());
        let result = result.unwrap();
        assert_eq!(result.message,       "win the game");
        assert_eq!(result.time.hour(),   at_5pm.hour());
        assert_eq!(result.time.minute(), 0);
        if Local::now().hour() > 12+5 {
            assert_eq!(result.time.day(), Local::now().day()+1);
        }
        else {
            assert_eq!(result.time.day(), Local::now().day());
        }

        let result = bot.parse_message("win the game at 5:00pm");
        assert!(result.is_some());
        let result = result.unwrap();
        assert_eq!(result.message,       "win the game");
        assert_eq!(result.time.hour(),   at_5pm.hour());
        assert_eq!(result.time.minute(), 0);
        if Local::now().hour() > 12+5 {
            assert_eq!(result.time.day(), Local::now().day()+1);
        }
        else {
            assert_eq!(result.time.day(), Local::now().day());
        }

        let result = bot.parse_message("remind me of a game at 5:30 AM");
        assert!(result.is_some());
        let result = result.unwrap();
        assert_eq!(result.message,       "a game");
        assert_eq!(result.time.hour(),   at_5_30am.hour());
        assert_eq!(result.time.minute(), at_5_30am.minute());
        if Local::now().hour() > 5 {
            assert_eq!(result.time.day(), Local::now().day()+1);
        }
        else {
            assert_eq!(result.time.day(), Local::now().day());
        }
    }

    #[test]
    fn bot_can_parse_on_monday_at_5pm() {
        let (bot, _) = Bot::new(":memory:")
            .expect("Couldn't create bot");

        let at_5pm = Local::now().with_hour(12+5).unwrap().with_minute(0).unwrap();
        let at_5am = Local::now().with_hour(5).unwrap().with_minute(0).unwrap();

        let result = bot.parse_message("remind me to do a solid on Monday at 5:00 pm");
        assert!(result.is_some());
        let result = result.unwrap();
        assert_eq!(result.message,        "do a solid");
        assert_eq!(result.time.weekday(), Weekday::Mon);
        assert_eq!(result.time.hour(),    at_5pm.hour());
        assert_eq!(result.time.minute(),  at_5pm.minute());

        let result = bot.parse_message("remind me of the test at 5:00am on Tuesday");
        assert!(result.is_some());
        let result = result.unwrap();
        assert_eq!(result.message, "the test");
        assert_eq!(result.time.weekday(), Weekday::Tue);
        assert_eq!(result.time.hour(),    at_5am.hour());
        assert_eq!(result.time.minute(),  at_5am.minute());
    }

    #[test]
    fn bot_can_parse_and_reject_nonsense() {
        let (bot, _) = Bot::new(":memory:")
            .expect("Couldn't create bot");

        let result = bot.parse_message("what if I want to remind me to die at Monday on 5:00pm");
        assert!(result.is_none());

        // TODO not crucial
        //let result = bot.parse_message("at 5:00pm");
        //assert!(result.is_none());

        let result = bot.parse_message("remind me to die at 100000:00pm");
        assert!(result.is_none());

        let result = bot.parse_message("bring it on");
        assert!(result.is_none());

        let result = bot.parse_message("when do remind me of to 1am remind me on 1:022pm 1am");
        assert!(result.is_none());

        let result = bot.parse_message("1:00");
        assert!(result.is_none());

        // TODO also not crucial
        //let result = bot.parse_message("on saturday");
        //assert!(result.is_none());
    }

    #[test]
    fn bot_can_set_reminder_for_right_now() {
        let (mut bot, receiver) = Bot::new(":memory:")
            .expect("Couldn't create bot");
        bot.set_reminder_msg_time("Reminder string here", &Local::now())
            .expect("Couldn't set reminder");

        let reminder_str = receiver.recv_timeout(time::Duration::from_secs(30))
            .expect("Reminder not triggered!");

        assert_eq!(&reminder_str, "Reminder string here");
    }

    #[test]
    fn bot_can_set_reminder_for_the_future() {
        let (mut bot, receiver) = Bot::new(":memory:")
            .expect("Couldn't create bot");
        bot.set_reminder_msg_time("Reminder string here", &Local::now().add(chrono::Duration::seconds(10)))
            .expect("Couldn't set reminder");

        let reminder_str = receiver.recv_timeout(time::Duration::from_secs(30))
            .expect("Reminder not triggered!");

        assert_eq!(&reminder_str, "Reminder string here");
    }

    #[test]
    fn bot_can_set_multiple_reminders_for_the_future() {
        let (mut bot, receiver) = Bot::new(":memory:")
            .expect("Couldn't create bot");
        bot.set_reminder_msg_time("Reminder 1", &Local::now().add(chrono::Duration::seconds(3)))
            .expect("Couldn't set reminder");
        bot.set_reminder_msg_time("Reminder 2", &Local::now().add(chrono::Duration::seconds(5)))
            .expect("Couldn't set reminder");
        bot.set_reminder_msg_time("Reminder 6", &Local::now().add(chrono::Duration::seconds(13)))
            .expect("Couldn't set reminder");
        bot.set_reminder_msg_time("Reminder 3", &Local::now().add(chrono::Duration::seconds(7)))
            .expect("Couldn't set reminder");
        bot.set_reminder_msg_time("Reminder 4", &Local::now().add(chrono::Duration::seconds(9)))
            .expect("Couldn't set reminder");
        bot.set_reminder_msg_time("Reminder 5", &Local::now().add(chrono::Duration::seconds(9)))
            .expect("Couldn't set reminder");

        let reminder_str = receiver.recv_timeout(time::Duration::from_secs(30))
            .expect("Reminder not triggered!");
        assert_eq!(&reminder_str, "Reminder 1");

        let reminder_str = receiver.recv_timeout(time::Duration::from_secs(30))
            .expect("Reminder not triggered!");
        assert_eq!(&reminder_str, "Reminder 2");

        let reminder_str = receiver.recv_timeout(time::Duration::from_secs(30))
            .expect("Reminder not triggered!");
        assert_eq!(&reminder_str, "Reminder 3");

        let reminder_str = receiver.recv_timeout(time::Duration::from_secs(30))
            .expect("Reminder not triggered!");
        assert_eq!(&reminder_str, "Reminder 4");

        let reminder_str = receiver.recv_timeout(time::Duration::from_secs(30))
            .expect("Reminder not triggered!");
        assert_eq!(&reminder_str, "Reminder 5");

        let reminder_str = receiver.recv_timeout(time::Duration::from_secs(30))
            .expect("Reminder not triggered!");
        assert_eq!(&reminder_str, "Reminder 6");
    }

    #[test]
    fn bot_can_handle_message() {
        let (mut bot, receiver) = Bot::new(":memory:")
            .expect("Couldn't create bot");

        let confirmation_str = bot.handle_message("Remind me to see this message please in 3 seconds");

        assert!(confirmation_str.contains("OK"));

        let reminder_str = receiver.recv_timeout(time::Duration::from_secs(30))
            .expect("Reminder not triggered!");

        assert_eq!(&reminder_str, "see this message please");
    }
}
